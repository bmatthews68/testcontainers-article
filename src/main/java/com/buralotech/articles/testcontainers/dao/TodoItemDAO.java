package com.buralotech.articles.testcontainers.dao;

import com.buralotech.articles.testcontainers.dto.UpdateAction;
import com.buralotech.articles.testcontainers.dto.TodoItemDTO;
import com.buralotech.articles.testcontainers.dto.TodoItemPriority;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface TodoItemDAO {

    List<TodoItemDTO> findAll(String ownerId, int start, int limit);

    Optional<TodoItemDTO> findOne(String ownerId, String id);

    TodoItemDTO create(String ownerId, String id, String description, TodoItemPriority priority, LocalDate dueDate);

    void update(String ownerId, String id, List<UpdateAction> updates);

    void delete(String ownerId, String id);

    void purge(String ownerId);
}
