package com.buralotech.articles.testcontainers.dao.jdbc;

import com.buralotech.articles.testcontainers.dao.TodoItemDAO;
import com.buralotech.articles.testcontainers.dto.*;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;


// tag::article[]
@Repository
public class JDBCTodoItemDAO implements TodoItemDAO {

    private final JdbcOperations jdbcTemplate;

    public JDBCTodoItemDAO(final JdbcOperations jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    // end::article[]

    @Override
    public List<TodoItemDTO> findAll(final String ownerId,
                                     final int start,
                                     final int limit) {
        return jdbcTemplate.query("""
                        SELECT ID, DESCRIPTION, PRIORITY, DUE_DATE, STATUS
                        FROM TODOITEMS
                        WHERE OWNER_ID = ?
                        ORDER BY ID
                        LIMIT ? OFFSET ?
                        """,
                ps -> {
                    ps.setString(1, ownerId);
                    ps.setInt(2, limit);
                    ps.setInt(3, start);
                },
                this::fromRow);
    }

    // tag::article[]
    @Override
    public Optional<TodoItemDTO> findOne(final String ownerId,
                                         final String id) {
        return jdbcTemplate.query("""
                                SELECT ID, DESCRIPTION, PRIORITY, DUE_DATE, STATUS
                                FROM TODOITEMS
                                WHERE OWNER_ID = ?
                                AND ID = ?
                                """,
                        ps -> {
                            ps.setString(1, ownerId);
                            ps.setString(2, id);
                        },
                        this::fromRow)
                .stream()
                .findFirst();
    }

    @Override
    public TodoItemDTO create(final String ownerId,
                              final String id,
                              final String description,
                              final TodoItemPriority priority,
                              final LocalDate dueDate) {
        jdbcTemplate.update("""
                        INSERT
                        INTO TODOITEMS (OWNER_ID, ID, DESCRIPTION, PRIORITY, DUE_DATE, STATUS)
                        VALUES (?, ?, ?, ?, ?, 0)
                        """,
                ps -> {
                    ps.setString(1, ownerId);
                    ps.setString(2, id);
                    ps.setString(3, description);
                    ps.setInt(4, priority.ordinal());
                    if (dueDate == null) {
                        ps.setNull(5, Types.DATE);
                    } else {
                        ps.setDate(5, fromLocalDate(dueDate));
                    }
                });
        return new TodoItemDTO(id, description, priority, dueDate, TodoItemStatus.NOT_STARTED);
    }

    // end::article[]

    @Override
    public void update(final String ownerId,
                       final String id,
                       final List<UpdateAction> updates) {
        final var changeDetailsAction = findAction(updates, ChangeDetailsAction.class);
        final var changeStatusAction = findAction(updates, ChangeStatusAction.class);
        if (changeDetailsAction.isPresent() && changeStatusAction.isPresent()) {
            jdbcTemplate.update("""
                            UPDATE TODOITEMS
                            SET DESCRIPION = ?,
                                PRIORITY = ?,
                                DUE_DATE = ?,
                                STATUS = ?
                            WHERE OWNER_ID = ?
                            AND ID = ?
                            """,
                    ps -> {
                        ps.setString(1, changeDetailsAction.get().description());
                        ps.setInt(2, changeDetailsAction.get().priority().ordinal());
                        ps.setDate(3, fromLocalDate(changeDetailsAction.get().dueDate()));
                        ps.setInt(4, changeStatusAction.get().status().ordinal());
                        ps.setString(5, ownerId);
                        ps.setString(6, id);
                    });
        } else {
            changeDetailsAction.ifPresent(action ->
                    jdbcTemplate.update("""
                                    UPDATE TODOITEMS
                                    SET DESCRIPION = ?,
                                        PRIORITY = ?,
                                        DUE_DATE = ?
                                    WHERE OWNER_ID = ?
                                    AND ID = ?
                                    """,
                            ps -> {
                                ps.setString(1, changeDetailsAction.get().description());
                                ps.setInt(2, changeDetailsAction.get().priority().ordinal());
                                ps.setDate(3, fromLocalDate(changeDetailsAction.get().dueDate()));
                                ps.setString(4, ownerId);
                                ps.setString(5, id);
                            }));
            changeStatusAction.ifPresent(action ->
                    jdbcTemplate.update("""
                                    UPDATE TODOITEMS
                                    SET STATUS = ?
                                    WHERE OWNER_ID = ?
                                    AND ID = ?
                                    """,
                            ps -> {
                                ps.setInt(1, changeStatusAction.get().status().ordinal());
                                ps.setString(2, ownerId);
                                ps.setString(3, id);
                            }));
        }
    }

    private <T extends UpdateAction> Optional<T> findAction(final List<UpdateAction> updates,
                                                            final Class<T> clazz) {
        return updates.stream().filter(clazz::isInstance).findFirst().map(clazz::cast);
    }

    // tag::article[]
    @Override
    public void delete(final String ownerId,
                       final String id) {
        jdbcTemplate.update("""
                        DELETE FROM TODOITEMS
                        WHERE OWNER_ID = ?
                        AND ID = ?
                        """,
                ps -> {
                    ps.setString(1, ownerId);
                    ps.setString(2, id);
                });
    }

    // end::article[]

    @Override
    public void purge(final String ownerId) {
        jdbcTemplate.update("""
                        DELETE FROM TODOITEMS
                        WHERE OWNER_ID = ?
                        """,
                ps -> ps.setString(1, ownerId));
    }

    // tag::article[]
    private TodoItemDTO fromRow(final ResultSet rs,
                                final int idx)
            throws SQLException {
        final var id = rs.getString(1);
        final var description = rs.getString(2);
        final var priority = rs.getInt(3);
        final var dueDate = rs.getDate(4);
        final var status = rs.getInt(5);
        return new TodoItemDTO(
                id,
                description,
                TodoItemPriority.values()[priority],
                dueDate == null ? null : LocalDate.ofInstant(dueDate.toInstant(), ZoneOffset.UTC),
                TodoItemStatus.values()[status]);
    }

    private Date fromLocalDate(final LocalDate localDate) {
        return new Date(localDate.toEpochSecond(LocalTime.MIDNIGHT, ZoneOffset.UTC));
    }
}
// end::article[]
