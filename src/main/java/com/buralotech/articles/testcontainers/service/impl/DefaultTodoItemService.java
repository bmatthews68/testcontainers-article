package com.buralotech.articles.testcontainers.service.impl;

import com.buralotech.articles.testcontainers.dao.TodoItemDAO;
import com.buralotech.articles.testcontainers.dto.TodoItemDTO;
import com.buralotech.articles.testcontainers.dto.TodoItemPriority;
import com.buralotech.articles.testcontainers.dto.UpdateAction;
import com.buralotech.articles.testcontainers.service.IdentifierService;
import com.buralotech.articles.testcontainers.service.TodoItemService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service    
public class DefaultTodoItemService implements TodoItemService {

    private final IdentifierService identifierService;

    private final TodoItemDAO todoItemDao;

    public DefaultTodoItemService(final IdentifierService identifierService,
                                  final TodoItemDAO todoItemDao) {
        this.identifierService = identifierService;
        this.todoItemDao = todoItemDao;
    }

    @Transactional(readOnly = true)
    @Override
    public List<TodoItemDTO> list(final String ownerId,
                                  final int start,
                                  final int limit) {
        return todoItemDao.findAll(ownerId, start, limit);
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<TodoItemDTO> get(final String ownerId,
                                     final String id) {
        return todoItemDao.findOne(ownerId, id);
    }

    @Transactional
    @Override
    public TodoItemDTO create(final String ownerId,
                              final String description,
                              final TodoItemPriority priority,
                              final LocalDate dueDate) {
        final var id = identifierService.generateIdentifier();
        return todoItemDao.create(ownerId, id, description, priority, dueDate);
    }

    @Transactional
    @Override
    public void update(final String ownerId,
                       final String id,
                       final List<UpdateAction> actions) {
        todoItemDao.update(ownerId, id, actions);
    }

    @Transactional
    @Override
    public void delete(final String ownerId,
                       final String id) {
        todoItemDao.delete(ownerId, id);
    }

    @Transactional
    @Override
    public void purge(final String ownerId) {
        todoItemDao.purge(ownerId);
    }
}
