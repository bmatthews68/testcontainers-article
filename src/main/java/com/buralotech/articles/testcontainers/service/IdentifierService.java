package com.buralotech.articles.testcontainers.service;

public interface IdentifierService {

    String generateIdentifier();
}
