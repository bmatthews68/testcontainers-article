package com.buralotech.articles.testcontainers.service;

import com.buralotech.articles.testcontainers.dto.TodoItemDTO;
import com.buralotech.articles.testcontainers.dto.TodoItemPriority;
import com.buralotech.articles.testcontainers.dto.UpdateAction;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface TodoItemService {

    List<TodoItemDTO> list(String ownerId, int start, int limit);

    Optional<TodoItemDTO> get(String ownerId, String id);

    TodoItemDTO create(String ownerId, String description, TodoItemPriority priority, LocalDate dueDate);

    void update(String ownerId, String id, List<UpdateAction> actions);

    void delete(String ownerId, String id);

    void purge(String ownerId);
}
