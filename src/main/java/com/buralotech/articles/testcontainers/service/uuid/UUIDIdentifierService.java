package com.buralotech.articles.testcontainers.service.uuid;

import com.buralotech.articles.testcontainers.service.IdentifierService;
import com.fasterxml.uuid.EthernetAddress;
import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.NoArgGenerator;
import com.fasterxml.uuid.impl.UUIDUtil;
import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
public class UUIDIdentifierService implements IdentifierService {

    private final NoArgGenerator generator = Generators.timeBasedReorderedGenerator(EthernetAddress.fromInterface());

    private final Base64.Encoder encoder = Base64.getUrlEncoder().withoutPadding();

    @Override
    public String generateIdentifier() {
        return encoder.encodeToString(UUIDUtil.asByteArray(generator.generate()));
    }
}
