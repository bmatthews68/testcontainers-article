package com.buralotech.articles.testcontainers.dto;

public sealed interface UpdateAction permits ChangeDetailsAction, ChangeStatusAction {
}
