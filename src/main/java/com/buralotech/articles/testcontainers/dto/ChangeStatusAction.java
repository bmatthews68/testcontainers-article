package com.buralotech.articles.testcontainers.dto;

public record ChangeStatusAction(TodoItemStatus status) implements UpdateAction {

}
