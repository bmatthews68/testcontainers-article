package com.buralotech.articles.testcontainers.dto;

import java.time.LocalDate;

// tag::article[]
public record TodoItemDTO(String id, String description, TodoItemPriority priority, LocalDate dueDate,
                          TodoItemStatus status) {

}
// end::article[]
