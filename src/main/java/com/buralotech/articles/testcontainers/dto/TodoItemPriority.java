package com.buralotech.articles.testcontainers.dto;

public enum TodoItemPriority {
    URGENT,
    HIGH,
    MEDIUM,
    LOW
}
