package com.buralotech.articles.testcontainers.dto;

import java.time.LocalDate;

public record ChangeDetailsAction(String description, TodoItemPriority priority,
                                  LocalDate dueDate) implements UpdateAction {

}
