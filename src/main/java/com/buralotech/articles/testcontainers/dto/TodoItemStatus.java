package com.buralotech.articles.testcontainers.dto;

public enum TodoItemStatus {
    NOT_STARTED,
    IN_PROGRESS,
    DONE
}
