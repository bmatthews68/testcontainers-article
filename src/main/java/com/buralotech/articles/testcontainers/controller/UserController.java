package com.buralotech.articles.testcontainers.controller;

import com.buralotech.articles.testcontainers.service.TodoItemService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users/{owner}")
public class UserController {

    private final TodoItemService todoItemService;

    public UserController(final TodoItemService todoItemService) {
        this.todoItemService = todoItemService;
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteItem(@PathVariable("owner") final String ownerId) {
        todoItemService.purge(ownerId);
        return ResponseEntity.noContent().build();
    }
}
