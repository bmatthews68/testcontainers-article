package com.buralotech.articles.testcontainers.controller;

import com.buralotech.articles.testcontainers.dto.ChangeDetailsAction;
import com.buralotech.articles.testcontainers.dto.ChangeStatusAction;
import com.buralotech.articles.testcontainers.dto.TodoItemDTO;
import com.buralotech.articles.testcontainers.dto.TodoItemStatus;
import com.buralotech.articles.testcontainers.payload.TodoItemRequest;
import com.buralotech.articles.testcontainers.service.TodoItemService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/users/{owner}/items")
public class TodoItemController {

    private final TodoItemService todoItemService;

    public TodoItemController(final TodoItemService todoItemService) {
        this.todoItemService = todoItemService;
    }

    @GetMapping
    public ResponseEntity<List<TodoItemDTO>> findItems(@PathVariable("owner") final String ownerId,
                                                       @RequestParam(value = "start", defaultValue = "0") final int start,
                                                       @RequestParam(value = "limit", defaultValue = "100") final int limit) {
        return ResponseEntity.ok(todoItemService.list(ownerId, start, limit));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TodoItemDTO> createItem(@PathVariable("owner") final String ownerId,
                                                  final UriComponentsBuilder uriComponentsBuilder,
                                                  @RequestBody final TodoItemRequest request) {

        final var newItem = todoItemService.create(ownerId, request.description(), request.priority(), request.dueDate());
        return ResponseEntity
                .created(uriComponentsBuilder.path("/api/users/{owner}/items/{id}").buildAndExpand(ownerId, newItem.id()).toUri())
                .body(newItem);
    }

    @GetMapping("{item}")
    public ResponseEntity<TodoItemDTO> getItem(@PathVariable("owner") final String ownerId,
                                               @PathVariable("item") final String id) {
        return todoItemService.get(ownerId, id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PatchMapping("{item}")
    public ResponseEntity<Void> updateItem(@PathVariable("owner") final String ownerId,
                                           @PathVariable("item") final String id,
                                           @RequestBody final TodoItemRequest request) {
        todoItemService.update(ownerId, id, Collections.singletonList(new ChangeDetailsAction(request.description(), request.priority(), request.dueDate())));
        return ResponseEntity.noContent().build();
    }

    @PutMapping("{item}/status")
    public ResponseEntity<Void> updateItemStatus(@PathVariable("owner") final String ownerId,
                                                 @PathVariable("item") final String id,
                                                 @RequestBody final TodoItemStatus status) {
        todoItemService.update(ownerId, id, Collections.singletonList(new ChangeStatusAction(status)));
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{item}")
    public ResponseEntity<Void> deleteItem(@PathVariable("owner") final String ownerId,
                                           @PathVariable("item") final String id) {
        todoItemService.delete(ownerId, id);
        return ResponseEntity.noContent().build();
    }
}
