package com.buralotech.articles.testcontainers.payload;

import com.buralotech.articles.testcontainers.dto.TodoItemPriority;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public record TodoItemRequest(String description, TodoItemPriority priority, LocalDate dueDate) {

    @SuppressWarnings("unused")
    @JsonCreator
    public TodoItemRequest(@JsonProperty("description") final String description,
                           @JsonProperty("priority") final TodoItemPriority priority,
                           @JsonProperty("dueDate") final LocalDate dueDate) {
        this.description = description;
        this.priority = priority;
        this.dueDate = dueDate;
    }
}
