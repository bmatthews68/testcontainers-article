package com.buralotech.articles.testcontainers.service.uuid;

import com.buralotech.articles.testcontainers.service.IdentifierService;
import org.junit.jupiter.api.Test;

import static com.buralotech.articles.testcontainers.common.Constants.ID_REGEX;
import static org.assertj.core.api.Assertions.assertThat;

public class UUIDIdentifierServiceTest {

    private final IdentifierService identifierService =  new UUIDIdentifierService();

    @Test
    void generateIdentifier() {
        assertThat(identifierService.generateIdentifier()).matches(ID_REGEX);
    }
}
