package com.buralotech.articles.testcontainers.service.impl;

import com.buralotech.articles.testcontainers.dao.TodoItemDAO;
import com.buralotech.articles.testcontainers.dto.TodoItemDTO;
import com.buralotech.articles.testcontainers.dto.TodoItemPriority;
import com.buralotech.articles.testcontainers.service.IdentifierService;
import com.buralotech.articles.testcontainers.service.TodoItemService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.transaction.PlatformTransactionManager;

import static com.buralotech.articles.testcontainers.common.Constants.DESCRIPTION;
import static com.buralotech.articles.testcontainers.common.Constants.ID;
import static com.buralotech.articles.testcontainers.common.Constants.OWNER_ID;
import static com.buralotech.articles.testcontainers.dto.TodoItemPriority.URGENT;
import static com.buralotech.articles.testcontainers.dto.TodoItemStatus.NOT_STARTED;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@AutoConfigureTestDatabase
public class DefaultTodoItemServiceTest {

    private final TodoItemDTO todoItem = new TodoItemDTO(ID, DESCRIPTION, URGENT, null, NOT_STARTED);

    @MockBean
    private TodoItemDAO todoItemDao;

    @MockBean
    private IdentifierService identifierService;

    @MockBean
    private PlatformTransactionManager transactionManager;

    @Autowired
    private TodoItemService todoItemService;

    @Test
    void createUrgentItemWithoutDueDate() {
        when(identifierService.generateIdentifier()).thenReturn(ID);
        when(todoItemDao.create(anyString(), anyString(), anyString(), any(), any())).thenReturn(todoItem);
        assertThat(todoItemService.create(OWNER_ID, DESCRIPTION, TodoItemPriority.URGENT, null)).isSameAs(todoItem);
        verify(identifierService).generateIdentifier();
        verify(todoItemDao).create(OWNER_ID, ID, DESCRIPTION, TodoItemPriority.URGENT, null);
        verifyNoMoreInteractions(identifierService, todoItemDao);
    }
}
