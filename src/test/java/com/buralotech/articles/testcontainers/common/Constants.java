package com.buralotech.articles.testcontainers.common;

public class Constants {

    public static final String ID_REGEX = "[a-zA-Z0-9_\\-]{22}";

    public static final String OWNER_ID = "IaSpoml9EeuJW5rSbD60iw";

    public static final String ID = "LZtBW2l9EeusTprSbD60iw";

    public static final String DESCRIPTION = "Wash the dog";

    public static final String EXISTING_OWNER_ID = "HdzxznN8EeunuD5h57thow";

    public static final String EXISTING_ID = "Hd0Y33N8EeunuD5h57thow";

    public static final String EXISTING_DESCRIPTION = "Mop the floors";

    private Constants() {
    }
}
