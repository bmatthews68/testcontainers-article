package com.buralotech.articles.testcontainers.dao.jdbc;

import com.buralotech.articles.testcontainers.dao.TodoItemDAO;
import com.buralotech.articles.testcontainers.dto.TodoItemPriority;
import com.buralotech.articles.testcontainers.dto.TodoItemStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import static com.buralotech.articles.testcontainers.common.Constants.DESCRIPTION;
import static com.buralotech.articles.testcontainers.common.Constants.ID;
import static com.buralotech.articles.testcontainers.common.Constants.OWNER_ID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

// tag::article[]
@ExtendWith(MockitoExtension.class) // <1>
class JDBCTodoItemDAOUnitTest {

    @Mock
    private JdbcTemplate jdbcTemplate; // <2>

    @Mock
    private PreparedStatement preparedStatement; // <3>

    private TodoItemDAO todoItemDao;

    @BeforeEach
    void setup() {
        todoItemDao = new JDBCTodoItemDAO(jdbcTemplate); // <4>
    }

    @Test
    void createUrgentItemWithoutDueDate() throws SQLException {
        when(jdbcTemplate.update(anyString(), any(PreparedStatementSetter.class))).thenAnswer(invocation -> { // <5>
            invocation.getArgument(1, PreparedStatementSetter.class).setValues(preparedStatement);
            return null;
        });
        final var todoItem = todoItemDao.create(OWNER_ID, ID, DESCRIPTION, TodoItemPriority.URGENT, null);
        assertThat(todoItem).isNotNull();
        assertThat(todoItem.id()).isEqualTo(ID);
        assertThat(todoItem.description()).isEqualTo(DESCRIPTION);
        assertThat(todoItem.priority()).isEqualTo(TodoItemPriority.URGENT);
        assertThat(todoItem.dueDate()).isNull();
        assertThat(todoItem.status()).isEqualTo(TodoItemStatus.NOT_STARTED);
        verify(jdbcTemplate).update(eq("INSERT\nINTO TODOITEMS (OWNER_ID, ID, DESCRIPTION, PRIORITY, DUE_DATE, STATUS)\nVALUES (?, ?, ?, ?, ?, 0)\n"), any(PreparedStatementSetter.class));
        verify(preparedStatement).setString(1, OWNER_ID);
        verify(preparedStatement).setString(2, ID);
        verify(preparedStatement).setString(3, DESCRIPTION);
        verify(preparedStatement).setInt(4, 0);
        verify(preparedStatement).setNull(5, Types.DATE);
        verifyNoMoreInteractions(jdbcTemplate, preparedStatement);
    }
}
// end::article[]
