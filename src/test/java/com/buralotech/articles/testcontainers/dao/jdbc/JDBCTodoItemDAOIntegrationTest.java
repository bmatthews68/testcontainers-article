package com.buralotech.articles.testcontainers.dao.jdbc;

import com.buralotech.articles.testcontainers.dao.TodoItemDAO;
import com.buralotech.articles.testcontainers.dto.TodoItemDTO;
import com.buralotech.articles.testcontainers.dto.TodoItemPriority;
import com.buralotech.articles.testcontainers.dto.TodoItemStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static com.buralotech.articles.testcontainers.common.Constants.DESCRIPTION;
import static com.buralotech.articles.testcontainers.common.Constants.EXISTING_DESCRIPTION;
import static com.buralotech.articles.testcontainers.common.Constants.EXISTING_ID;
import static com.buralotech.articles.testcontainers.common.Constants.EXISTING_OWNER_ID;
import static com.buralotech.articles.testcontainers.common.Constants.ID;
import static com.buralotech.articles.testcontainers.common.Constants.OWNER_ID;
import static org.assertj.core.api.Assertions.assertThat;

// tag::article[]
@DataJdbcTest(properties = { // <1>
        "spring.sql.init.mode=always" // <2>
})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE) // <3>
@Testcontainers // <4>
public class JDBCTodoItemDAOIntegrationTest {

    @Container // <5>
    private static final MariaDBContainer<?> mariaDbContainer = new MariaDBContainer<>("mariadb:10.5.8")  // <6>
            .withDatabaseName("todoitems") // <7>
            .withUsername("app")
            .withPassword("secret");

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private TodoItemDAO todoItemDao;

    @DynamicPropertySource // <8>
    static void properties(final DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", mariaDbContainer::getJdbcUrl); // <9>
    }

    @BeforeEach
    void setup() {
        todoItemDao = new JDBCTodoItemDAO(jdbcTemplate);
    }

    // end::article[]

    @Test
    void loadExistingItems() {
        var todoItems = todoItemDao.findAll(EXISTING_OWNER_ID, 0, 10);
        assertThat(todoItems).hasSize(10).element(0).satisfies(this::assertItem0);

        todoItems = todoItemDao.findAll(EXISTING_OWNER_ID, 10, 10);
        assertThat(todoItems).hasSize(1);

        todoItems = todoItemDao.findAll(EXISTING_OWNER_ID, 20, 10);
        assertThat(todoItems).hasSize(0);
    }

    // tag::article[]
    @Test
    void loadExistingItem() {
        final var optionalTodoItem = todoItemDao.findOne(EXISTING_OWNER_ID, EXISTING_ID);
        assertThat(optionalTodoItem).get().satisfies(this::assertItem0);
    }

    @Test
    void createUrgentItemWithoutDueDate() {
        final var todoItem = todoItemDao.create(OWNER_ID, ID, DESCRIPTION, TodoItemPriority.URGENT, null);
        assertThat(todoItem).isNotNull();
        assertThat(todoItem.id()).isEqualTo(ID);
        assertThat(todoItem.description()).isEqualTo(DESCRIPTION);
        assertThat(todoItem.priority()).isEqualTo(TodoItemPriority.URGENT);
        assertThat(todoItem.dueDate()).isNull();
        assertThat(todoItem.status()).isEqualTo(TodoItemStatus.NOT_STARTED);
    }

    @Test
    void deleteExistingItem() {
        todoItemDao.delete(EXISTING_OWNER_ID, EXISTING_ID);
        final var optionalTodoItem = todoItemDao.findOne(EXISTING_OWNER_ID, EXISTING_ID);
        assertThat(optionalTodoItem).isNotPresent();
    }

    // end::article[]

    @Test
    void deleteNonExistantItem() {
        todoItemDao.delete(OWNER_ID, ID);
    }

    // tag::article[]
    private void assertItem0(final TodoItemDTO todoItem) {
        assertThat(todoItem.id()).isEqualTo(EXISTING_ID);
        assertThat(todoItem.description()).isEqualTo(EXISTING_DESCRIPTION);
        assertThat(todoItem.priority()).isEqualTo(TodoItemPriority.MEDIUM);
        assertThat(todoItem.dueDate()).isNull();
        assertThat(todoItem.status()).isEqualTo(TodoItemStatus.NOT_STARTED);
    }
}
// end::article[]
