package com.buralotech.articles.testcontainers.controller;

import com.buralotech.articles.testcontainers.dto.TodoItemDTO;
import com.buralotech.articles.testcontainers.service.TodoItemService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static com.buralotech.articles.testcontainers.common.Constants.DESCRIPTION;
import static com.buralotech.articles.testcontainers.common.Constants.ID;
import static com.buralotech.articles.testcontainers.common.Constants.OWNER_ID;
import static com.buralotech.articles.testcontainers.dto.TodoItemPriority.URGENT;
import static com.buralotech.articles.testcontainers.dto.TodoItemStatus.NOT_STARTED;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpHeaders.LOCATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

// tag::article[]
@WebMvcTest // <1>
class TodoItemControllerUnitTest {

    private final TodoItemDTO todoItem = new TodoItemDTO(ID, DESCRIPTION, URGENT, null, NOT_STARTED);

    @Autowired
    private MockMvc mockMvc; // <2>

    @MockBean
    private TodoItemService todoItemService; // <3>

    @Test
    void createUrgentItemWithoutDueDate() throws Exception {
        when(todoItemService.create(anyString(), anyString(), any(), any())).thenReturn(todoItem);
        mockMvc
                .perform(
                        post("/api/users/{owner}/items", OWNER_ID)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(String.format("{\"description\":\"%s\",\"priority\":\"%s\"}", DESCRIPTION, URGENT)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string(LOCATION, String.format("http://localhost/api/users/%s/items/%s", OWNER_ID, ID)))
                .andExpect(jsonPath("$.id").value(ID))
                .andExpect(jsonPath("$.description").value(DESCRIPTION))
                .andExpect(jsonPath("$.priority").value(URGENT.name()))
                .andExpect(jsonPath("$.dueDate").doesNotExist())
                .andExpect(jsonPath("$.status").value(NOT_STARTED.name()));
        verify(todoItemService).create(OWNER_ID, DESCRIPTION, URGENT, null);
        verifyNoMoreInteractions(todoItemService);
    }
}
// end::article[]
