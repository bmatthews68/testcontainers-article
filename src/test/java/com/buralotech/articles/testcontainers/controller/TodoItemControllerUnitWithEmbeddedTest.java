package com.buralotech.articles.testcontainers.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static com.buralotech.articles.testcontainers.common.Constants.DESCRIPTION;
import static com.buralotech.articles.testcontainers.common.Constants.EXISTING_DESCRIPTION;
import static com.buralotech.articles.testcontainers.common.Constants.EXISTING_ID;
import static com.buralotech.articles.testcontainers.common.Constants.EXISTING_OWNER_ID;
import static com.buralotech.articles.testcontainers.common.Constants.ID;
import static com.buralotech.articles.testcontainers.common.Constants.ID_REGEX;
import static com.buralotech.articles.testcontainers.common.Constants.OWNER_ID;
import static com.buralotech.articles.testcontainers.dto.TodoItemPriority.MEDIUM;
import static com.buralotech.articles.testcontainers.dto.TodoItemPriority.URGENT;
import static com.buralotech.articles.testcontainers.dto.TodoItemStatus.NOT_STARTED;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.matchesRegex;
import static org.springframework.http.HttpHeaders.LOCATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

// tag::article[]
@SpringBootTest // <1>
@AutoConfigureTestDatabase
@AutoConfigureMockMvc // <2>
@Transactional // <3>
class TodoItemControllerUnitWithEmbeddedTest {

    @Autowired
    private MockMvc mockMvc; // <1>

    // end::article[]

    @Test
    void loadExistingItems() throws Exception {
        mockMvc.perform(
                get("/api/users/{owner}/items", EXISTING_OWNER_ID)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("start", "0")
                        .param("limit", "10"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(10)))
                .andExpect(jsonPath("$[0].id").value(EXISTING_ID))
                .andExpect(jsonPath("$[0].description").value(EXISTING_DESCRIPTION))
                .andExpect(jsonPath("$[0].priority").value(MEDIUM.name()))
                .andExpect(jsonPath("$[0].dueDate").doesNotExist())
                .andExpect(jsonPath("$[0].status").value(NOT_STARTED.name()));

        mockMvc.perform(
                get("/api/users/{owner}/items", EXISTING_OWNER_ID)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("start", "10")
                        .param("limit", "10"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(1)));

        mockMvc.perform(
                get("/api/users/{owner}/items", EXISTING_OWNER_ID)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("start", "20")
                        .param("limit", "10"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(0)));
    }

    // tag::article[]
    @Test
    void loadExistingItem() throws Exception {
        mockMvc.perform(
                get("/api/users/{owner}/items/{item}", EXISTING_OWNER_ID, EXISTING_ID)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(EXISTING_ID))
                .andExpect(jsonPath("$.description").value(EXISTING_DESCRIPTION))
                .andExpect(jsonPath("$.priority").value(MEDIUM.name()))
                .andExpect(jsonPath("$.dueDate").doesNotExist())
                .andExpect(jsonPath("$.status").value(NOT_STARTED.name()));
    }

    @Test
    void createUrgentItemWithoutDueDate() throws Exception {
        mockMvc
                .perform(
                        post("/api/users/{owner}/items", OWNER_ID)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(String.format("{\"description\":\"%s\",\"priority\":\"%s\"}", DESCRIPTION, URGENT)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string(LOCATION, matchesRegex(String.format("http://localhost/api/users/%s/items/[a-zA-Z0-9_\\-]{22}", OWNER_ID))))
                .andExpect(jsonPath("$.id").value(matchesRegex(ID_REGEX)))
                .andExpect(jsonPath("$.description").value(DESCRIPTION))
                .andExpect(jsonPath("$.priority").value(URGENT.name()))
                .andExpect(jsonPath("$.dueDate").doesNotExist())
                .andExpect(jsonPath("$.status").value(NOT_STARTED.name()));
    }

    @Test
    void deleteExistingItem() throws Exception {
        mockMvc.perform(
                delete("/api/users/{owner}/items/{item}", EXISTING_OWNER_ID, EXISTING_ID))
                .andDo(print())
                .andExpect(status().isNoContent());

    }

    // end::article[]

    @Test
    void deleteNonExistantItem() throws Exception {
        mockMvc.perform(
                delete("/api/users/{owner}/items/{item}", OWNER_ID, ID))
                .andDo(print())
                .andExpect(status().isNoContent());

    }

    // tag::article[]
}
// end::article[]
